# Code Golf Team Validator

This is a Code Golf challenge.
The task is to implement a function to validate the composition of some teams.
Implement the function with as few characters as possible!

## Usecase

The included iPhone app is intended to be used while constituting a group of teams to
easily find out which teams should be changed.

The app shows a list of newly constituted teams and for each team
it shows the team name and the social styles of the team members.

With a switch the user can filter the list, so that it shows only unbalanced teams.

## Balanced Teams?

A balanced team is a team which has at least one team member for each of the following
social styles:

- Analytical
- Driver
- Amiable
- Expressive

See 
<https://www.pmi.org/learning/library/personality-influences-way-address-challenges-6674>
for an explanation of the different types

## Implement

Implement the function in TeamValidator.swift

~~~
func findUnbalancedTeams(_ teamCompositions: [[String]]) -> [String] {
    // ... your implementation here
}
~~~

Input is an Array of team compositions. Each team composition contains the name of team
followed by the social styles of the team members.

The return value should be an array with the names of all unbalanced teams.

There is no need to deal with input error situations in the implementation, instead
you can assume that the input is always formally correct, that is

- Each team composition contains at least one element with the team name
- All other elements in a team composition are exactly on of
  "Analytical", "Driver", "Amiable" or "Expressive"

### Example usage

~~~
let input = [
    ["Team1", "Driver", "Analytical", "Amiable", "Expressive"],
    ["Team2", "Driver", "Analytical", "Amiable", "Driver", "Analytical", "Amiable"]
]

let result = findUnbalancedTeams(input)
// result should now contain ["Team2"]
~~~

## Test

- Run the Unit test to see if your implementation is correct.
- Run the iOS App to see if it works as expected.

## Deliver

Commit and push your implementation in a forked repository and create a merge request to participate.
You can provide updates on your solution with additional merge requests.

## Winner

- The winner is who delivers the solution with the fewest characters.
- All Unit test must be green.
- Only the body of the function (inside the curly brackets) counts.
- White space which is used only for indenting is not counted.


